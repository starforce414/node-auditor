/**
 * UserviewController
 *
 * @description :: Server-side logic for managing Userviews
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `UserviewController.dashboard()`
   */
  dashboard: function (req, res) {
    return res.view();
  },

  profile: function (req, res) {
    var id = req.session.user.id;

    User
      .findOne(id)
      .exec(function(err, model) {
        if(req.method == 'POST' && req.param('User', null) != null) {

          var data = req.param('User');

          if(data['firstname'] == '') {
            return res.view({err: 'Please input your First Name!', old: data});
          }
          if(data['lastname'] == '') {
            return res.view({err: 'Please input your Last Name!', old: data});
          }
          if(data['email'] == '') {
            return res.view({err: 'Please input your Email!', old: data});
          }
          if(data['old_password'] == '') {
            return res.view({err: 'Please input Old Password!', old: data});
          }
          if(data['password'] == '') {
            return res.view({err: 'Please input New Password!', old: data});
          }
          if(data['confirm_password'] == '') {
            return res.view({err: 'Please input Password Confirm!', old: data});
          }
          if(data['old_password'] != model.password) {
            return res.view({err: 'Old password is incorrect!', old: data});
          }
          if(data['password'] != data['confirm_password']) {
            return res.view({err: 'Password not match! Please input password correctly', old: data});
          }

          User
            .update({id: model.id}, data)
            .exec(function(err, model) {
              if(err) {
                return res.view({err: err.details, old: data});
              }
              else {
                return res.view({success: 'Profile updated!', old: model[0]});
              }
            });
        }
        else {
          return res.view({old: model});
        }
      });
  },

  usermanage: function (req, res) {
    res.locals.users = null;

    User
      .find()
      .where({ isAdmin: false })
      .exec(function(err, users) {
        res.locals.users = users;
        return res.view();
      });
  },

  adminmanage: function (req, res) {
    res.locals.users = null;

    User
      .find()
      .where({ isAdmin: true })
      .exec(function(err, users) {
        res.locals.users = users;
        return res.view();
      });
  },

  admincreate: function (req, res) {

    if(req.method == 'POST' && req.param('User', null) != null) {

      var data = req.param('User');

      if(data['firstname'] == '') {
        return res.view({err: 'Please input your First Name!', old: data});
      }
      if(data['lastname'] == '') {
        return res.view({err: 'Please input your Last Name!', old: data});
      }
      if(data['email'] == '') {
        return res.view({err: 'Please input your Email!', old: data});
      }
      if(data['password'] == '') {
        return res.view({err: 'Please input Password!', old: data});
      }
      if(data['confirm_password'] == '') {
        return res.view({err: 'Please input Password Confirm!', old: data});
      }
      if(data['password'] != data['confirm_password']) {
        return res.view({err: 'Password not match! Please input password correctly', old: data});
      }
      if(data['isAdmin'] == "on") {
        data['isAdmin'] = true;
      }
      User
        .create(data)
        .exec(function(err, model) {
          if(err) {
            return res.view({err: err.details, old: data});
          }
          else {
            res.redirect('/adminmanage');
          }
        });
    }
    else {
      return res.view();
    }
  },

  adminupdate: function (req, res) {

    var id = req.param('id', null);

    User
      .findOne(id)
      .exec(function(err, model) {
        if(req.method == 'POST' && req.param('User', null) != null) {

          var data = req.param('User');

          if(data['firstname'] == '') {
            return res.view({err: 'Please input your First Name!', old: data});
          }
          if(data['lastname'] == '') {
            return res.view({err: 'Please input your Last Name!', old: data});
          }
          if(data['email'] == '') {
            return res.view({err: 'Please input your Email!', old: data});
          }
          if(data['password'] == '') {
            return res.view({err: 'Please input Password!', old: data});
          }
          if(data['confirm_password'] == '') {
            return res.view({err: 'Please input Password Confirm!', old: data});
          }
          if(data['password'] != data['confirm_password']) {
            return res.view({err: 'Password not match! Please input password correctly', old: data});
          }
          if(data['isAdmin'] == "on") {
            data['isAdmin'] = true;
          } else {
            data['isAdmin'] = false;
          }
          User
            .update({id: model.id}, data)
            .exec(function(err, model) {
              if(err) {
                return res.view({err: err.details, old: data});
              }
              else {
                res.redirect('/adminmanage');
              }
            });
        }
        else {
          return res.view({old: model});
        }
      });
  },

  admindelete: function (req, res) {

    var id = req.param('id', null);

    User
      .findOne(id)
      .exec(function(err, model) {
        if(model) {

          User
            .destroy({id: model.id})
            .exec(function(err) {
              if(err) {
                res.redirect('/adminmanage');
              }
              else {
                res.redirect('/adminmanage');
              }
            });
        }
        else {
          res.redirect('/adminmanage');
        }
      });
  },

  usercreate: function (req, res) {

    if(req.method == 'POST' && req.param('User', null) != null) {

      var data = req.param('User');

      if(data['firstname'] == '') {
        return res.view({err: 'Please input your First Name!', old: data});
      }
      if(data['lastname'] == '') {
        return res.view({err: 'Please input your Last Name!', old: data});
      }
      if(data['email'] == '') {
        return res.view({err: 'Please input your Email!', old: data});
      }
      if(data['password'] == '') {
        return res.view({err: 'Please input Password!', old: data});
      }
      if(data['confirm_password'] == '') {
        return res.view({err: 'Please input Password Confirm!', old: data});
      }
      if(data['password'] != data['confirm_password']) {
        return res.view({err: 'Password not match! Please input password correctly', old: data});
      }
      if(data['isAdmin'] == "on") {
        data['isAdmin'] = true;
      }
      User
        .create(data)
        .exec(function(err, model) {
          if(err) {
            return res.view({err: err.details, old: data});
          }
          else {
            res.redirect('/usermanage');
          }
        });
    }
    else {
      return res.view();
    }
  },

  userupdate: function (req, res) {

    var id = req.param('id', null);

    User
      .findOne(id)
      .exec(function(err, model) {
        if(req.method == 'POST' && req.param('User', null) != null) {

          var data = req.param('User');

          if(data['firstname'] == '') {
            return res.view({err: 'Please input your First Name!', old: data});
          }
          if(data['lastname'] == '') {
            return res.view({err: 'Please input your Last Name!', old: data});
          }
          if(data['email'] == '') {
            return res.view({err: 'Please input your Email!', old: data});
          }
          if(data['password'] == '') {
            return res.view({err: 'Please input Password!', old: data});
          }
          if(data['confirm_password'] == '') {
            return res.view({err: 'Please input Password Confirm!', old: data});
          }
          if(data['password'] != data['confirm_password']) {
            return res.view({err: 'Password not match! Please input password correctly', old: data});
          }
          if(data['isAdmin'] == "on") {
            data['isAdmin'] = true;
          } else {
            data['isAdmin'] = false;
          }
          User
            .update({id: model.id}, data)
            .exec(function(err, model) {
              if(err) {
                return res.view({err: err.details, old: data});
              }
              else {
                res.redirect('/usermanage');
              }
            });
        }
        else {
          return res.view({old: model});
        }
      });
  },

  userdelete: function (req, res) {

    var id = req.param('id', null);

    User
      .findOne(id)
      .exec(function(err, model) {
        if(model) {

          User
            .destroy({id: model.id})
            .exec(function(err) {
              if(err) {
                res.redirect('/usermanage');
              }
              else {
                res.redirect('/usermanage');
              }
            });
        }
        else {
          res.redirect('/usermanage');
        }
      });
  },

};

