/**
 * AuthviewControllerController
 *
 * @description :: Server-side logic for managing Authviewcontrollers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  initadmin: function(req, res) {

    User
      .findOne()
      .where({ email: 'admin@admin.com' })
      .exec(function(err, user) {
        if(user) {
          return res.send('aleady exist');
        }

        var data = {
          firstname: 'admin',
          lastname: 'user',
          email: 'admin@admin.com',
          isAdmin: true,
          password: 'admin'
        };

        User
          .create(data)
          .exec(function(err, created) {
            if(err || !created) {
              return res.send('cannot create user');
            }

            return res.send('created');
          });
      });
  },

  login: function (req, res) {
    return res.view();
  },

  doLogin: function(req, res) {
    var email = req.body.email;
    var password = req.body.password;

    if(email === '' || password === '') {
      res.locals.error = 'please input field';
      return res.view('authview/login');
    }

    User
      .findOne()
      .where({ email: email, password: password })
      .exec(function(err, user) {
        if(err || !user) {
          res.locals.error = 'donot exist eamil and password';
          return res.view('authview/login');
        }
        req.session.user = user;
        return res.redirect('/dashboard');
      });
  },

  logout: function (req, res) {
    delete req.session.user;
    return res.redirect('/login');
  }
};

